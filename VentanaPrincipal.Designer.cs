﻿using System.Drawing;
using System.Windows.Forms;

namespace Gestion_Alumnos_Profesorado_Asignaturas
{
    partial class VentanaPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentanaPrincipal));
            this.barraMenu = new System.Windows.Forms.MenuStrip();
            this.menu_archivo = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuLateral = new System.Windows.Forms.Panel();
            this.labelSalir = new System.Windows.Forms.Label();
            this.labelCategoriaProfesor = new System.Windows.Forms.Label();
            this.labelCategoriaAsignatura = new System.Windows.Forms.Label();
            this.labelCategoriaAlumno = new System.Windows.Forms.Label();
            this.labelCategoriaInicio = new System.Windows.Forms.Label();
            this.PanelPrincipal = new System.Windows.Forms.Panel();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.barraMenu.SuspendLayout();
            this.MenuLateral.SuspendLayout();
            this.SuspendLayout();
            // 
            // barraMenu
            // 
            this.barraMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(139)))), ((int)(((byte)(168)))));
            this.barraMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_archivo});
            this.barraMenu.Location = new System.Drawing.Point(0, 0);
            this.barraMenu.Name = "barraMenu";
            this.barraMenu.Size = new System.Drawing.Size(726, 24);
            this.barraMenu.TabIndex = 0;
            this.barraMenu.Text = "menuStrip1";
            // 
            // menu_archivo
            // 
            this.menu_archivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.menu_archivo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.menu_archivo.Name = "menu_archivo";
            this.menu_archivo.Size = new System.Drawing.Size(60, 20);
            this.menu_archivo.Text = "Archivo";
            // 
            // MenuLateral
            // 
            this.MenuLateral.AutoScroll = true;
            this.MenuLateral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.MenuLateral.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MenuLateral.Controls.Add(this.labelSalir);
            this.MenuLateral.Controls.Add(this.labelCategoriaProfesor);
            this.MenuLateral.Controls.Add(this.labelCategoriaAsignatura);
            this.MenuLateral.Controls.Add(this.labelCategoriaAlumno);
            this.MenuLateral.Controls.Add(this.labelCategoriaInicio);
            this.MenuLateral.Dock = System.Windows.Forms.DockStyle.Left;
            this.MenuLateral.Location = new System.Drawing.Point(0, 24);
            this.MenuLateral.Name = "MenuLateral";
            this.MenuLateral.Size = new System.Drawing.Size(171, 414);
            this.MenuLateral.TabIndex = 1;
            // 
            // labelSalir
            // 
            this.labelSalir.BackColor = System.Drawing.Color.Red;
            this.labelSalir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSalir.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelSalir.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelSalir.Image = global::Gestion_Alumnos_Profesorado_Asignaturas.Properties.Resources.Close_White_16x;
            this.labelSalir.Location = new System.Drawing.Point(0, 373);
            this.labelSalir.Name = "labelSalir";
            this.labelSalir.Size = new System.Drawing.Size(169, 39);
            this.labelSalir.TabIndex = 13;
            this.labelSalir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSalir.Click += new System.EventHandler(this.labelSalir_Click);
            // 
            // labelCategoriaProfesor
            // 
            this.labelCategoriaProfesor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.labelCategoriaProfesor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCategoriaProfesor.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelCategoriaProfesor.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCategoriaProfesor.Image = ((System.Drawing.Image)(resources.GetObject("labelCategoriaProfesor.Image")));
            this.labelCategoriaProfesor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelCategoriaProfesor.Location = new System.Drawing.Point(0, 201);
            this.labelCategoriaProfesor.Name = "labelCategoriaProfesor";
            this.labelCategoriaProfesor.Padding = new System.Windows.Forms.Padding(25, 0, 25, 0);
            this.labelCategoriaProfesor.Size = new System.Drawing.Size(169, 67);
            this.labelCategoriaProfesor.TabIndex = 12;
            this.labelCategoriaProfesor.Text = "Profesores";
            this.labelCategoriaProfesor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelCategoriaProfesor.Click += new System.EventHandler(this.ItemCategoria_Click);
            this.labelCategoriaProfesor.MouseEnter += new System.EventHandler(this.ItemCategoria_Hover);
            this.labelCategoriaProfesor.MouseLeave += new System.EventHandler(this.ItemCategoria_HoverOut);
            // 
            // labelCategoriaAsignatura
            // 
            this.labelCategoriaAsignatura.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.labelCategoriaAsignatura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCategoriaAsignatura.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelCategoriaAsignatura.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCategoriaAsignatura.Image = ((System.Drawing.Image)(resources.GetObject("labelCategoriaAsignatura.Image")));
            this.labelCategoriaAsignatura.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelCategoriaAsignatura.Location = new System.Drawing.Point(0, 134);
            this.labelCategoriaAsignatura.Name = "labelCategoriaAsignatura";
            this.labelCategoriaAsignatura.Padding = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.labelCategoriaAsignatura.Size = new System.Drawing.Size(169, 67);
            this.labelCategoriaAsignatura.TabIndex = 11;
            this.labelCategoriaAsignatura.Text = "Asignaturas";
            this.labelCategoriaAsignatura.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelCategoriaAsignatura.Click += new System.EventHandler(this.ItemCategoria_Click);
            this.labelCategoriaAsignatura.MouseEnter += new System.EventHandler(this.ItemCategoria_Hover);
            this.labelCategoriaAsignatura.MouseLeave += new System.EventHandler(this.ItemCategoria_HoverOut);
            // 
            // labelCategoriaAlumno
            // 
            this.labelCategoriaAlumno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.labelCategoriaAlumno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCategoriaAlumno.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelCategoriaAlumno.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCategoriaAlumno.Image = ((System.Drawing.Image)(resources.GetObject("labelCategoriaAlumno.Image")));
            this.labelCategoriaAlumno.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelCategoriaAlumno.Location = new System.Drawing.Point(0, 67);
            this.labelCategoriaAlumno.Name = "labelCategoriaAlumno";
            this.labelCategoriaAlumno.Padding = new System.Windows.Forms.Padding(25, 0, 25, 0);
            this.labelCategoriaAlumno.Size = new System.Drawing.Size(169, 67);
            this.labelCategoriaAlumno.TabIndex = 10;
            this.labelCategoriaAlumno.Text = "Alumnos";
            this.labelCategoriaAlumno.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelCategoriaAlumno.Click += new System.EventHandler(this.ItemCategoria_Click);
            this.labelCategoriaAlumno.MouseEnter += new System.EventHandler(this.ItemCategoria_Hover);
            this.labelCategoriaAlumno.MouseLeave += new System.EventHandler(this.ItemCategoria_HoverOut);
            // 
            // labelCategoriaInicio
            // 
            this.labelCategoriaInicio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.labelCategoriaInicio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCategoriaInicio.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelCategoriaInicio.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCategoriaInicio.Image = ((System.Drawing.Image)(resources.GetObject("labelCategoriaInicio.Image")));
            this.labelCategoriaInicio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelCategoriaInicio.Location = new System.Drawing.Point(0, 0);
            this.labelCategoriaInicio.Name = "labelCategoriaInicio";
            this.labelCategoriaInicio.Padding = new System.Windows.Forms.Padding(35, 0, 35, 0);
            this.labelCategoriaInicio.Size = new System.Drawing.Size(169, 67);
            this.labelCategoriaInicio.TabIndex = 9;
            this.labelCategoriaInicio.Text = "Inicio";
            this.labelCategoriaInicio.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelCategoriaInicio.Click += new System.EventHandler(this.ItemCategoria_Click);
            this.labelCategoriaInicio.MouseEnter += new System.EventHandler(this.ItemCategoria_Hover);
            this.labelCategoriaInicio.MouseLeave += new System.EventHandler(this.ItemCategoria_HoverOut);
            // 
            // PanelPrincipal
            // 
            this.PanelPrincipal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.PanelPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelPrincipal.Location = new System.Drawing.Point(171, 24);
            this.PanelPrincipal.Name = "PanelPrincipal";
            this.PanelPrincipal.Size = new System.Drawing.Size(555, 414);
            this.PanelPrincipal.TabIndex = 2;
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // VentanaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 438);
            this.Controls.Add(this.PanelPrincipal);
            this.Controls.Add(this.MenuLateral);
            this.Controls.Add(this.barraMenu);
            this.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.barraMenu;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MinimumSize = new System.Drawing.Size(599, 452);
            this.Name = "VentanaPrincipal";
            this.Text = "Gestión Educativa";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ventanaPrincipal_FormClosing);
            this.barraMenu.ResumeLayout(false);
            this.barraMenu.PerformLayout();
            this.MenuLateral.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip barraMenu;
        private System.Windows.Forms.ToolStripMenuItem menu_archivo;
        private Panel MenuLateral;
        private Label labelSalir;
        private Label labelCategoriaProfesor;
        private Label labelCategoriaAsignatura;
        private Label labelCategoriaAlumno;
        private Label labelCategoriaInicio;
        private Panel PanelPrincipal;
        private ToolStripMenuItem salirToolStripMenuItem;
    }
}

