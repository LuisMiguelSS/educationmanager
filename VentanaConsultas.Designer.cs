﻿namespace Gestion_Alumnos_Profesorado_Asignaturas
{
    partial class VentanaConsultas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentanaConsultas));
            this.LabelTituloConsultar = new System.Windows.Forms.Label();
            this.DialogoAbrirArchivo = new System.Windows.Forms.OpenFileDialog();
            this.BtnAbrirArchivo = new System.Windows.Forms.Button();
            this.GrupoResultado = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.LabelIdentificador = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.GrupoResultado.SuspendLayout();
            this.SuspendLayout();
            // 
            // LabelTituloConsultar
            // 
            this.LabelTituloConsultar.AutoSize = true;
            this.LabelTituloConsultar.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTituloConsultar.Location = new System.Drawing.Point(167, 45);
            this.LabelTituloConsultar.Name = "LabelTituloConsultar";
            this.LabelTituloConsultar.Size = new System.Drawing.Size(131, 31);
            this.LabelTituloConsultar.TabIndex = 0;
            this.LabelTituloConsultar.Text = "Consultar";
            // 
            // DialogoAbrirArchivo
            // 
            this.DialogoAbrirArchivo.FileName = "openFileDialog1";
            // 
            // BtnAbrirArchivo
            // 
            this.BtnAbrirArchivo.Location = new System.Drawing.Point(166, 111);
            this.BtnAbrirArchivo.Name = "BtnAbrirArchivo";
            this.BtnAbrirArchivo.Size = new System.Drawing.Size(132, 34);
            this.BtnAbrirArchivo.TabIndex = 1;
            this.BtnAbrirArchivo.Text = "Seleccionar Archivo";
            this.BtnAbrirArchivo.UseVisualStyleBackColor = true;
            this.BtnAbrirArchivo.Click += new System.EventHandler(this.BtnAbrirArchivo_Click);
            // 
            // GrupoResultado
            // 
            this.GrupoResultado.Controls.Add(this.label1);
            this.GrupoResultado.Controls.Add(this.LabelIdentificador);
            this.GrupoResultado.Controls.Add(this.comboBox1);
            this.GrupoResultado.Enabled = false;
            this.GrupoResultado.Location = new System.Drawing.Point(31, 161);
            this.GrupoResultado.Name = "GrupoResultado";
            this.GrupoResultado.Size = new System.Drawing.Size(412, 259);
            this.GrupoResultado.TabIndex = 2;
            this.GrupoResultado.TabStop = false;
            this.GrupoResultado.Text = "Información";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(188, 19);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // LabelIdentificador
            // 
            this.LabelIdentificador.AutoSize = true;
            this.LabelIdentificador.Location = new System.Drawing.Point(115, 22);
            this.LabelIdentificador.Name = "LabelIdentificador";
            this.LabelIdentificador.Size = new System.Drawing.Size(19, 13);
            this.LabelIdentificador.TabIndex = 1;
            this.LabelIdentificador.Text = "Id:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(55, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(332, 156);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // VentanaConsultas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.ClientSize = new System.Drawing.Size(477, 432);
            this.Controls.Add(this.GrupoResultado);
            this.Controls.Add(this.BtnAbrirArchivo);
            this.Controls.Add(this.LabelTituloConsultar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(493, 471);
            this.MinimumSize = new System.Drawing.Size(493, 471);
            this.Name = "VentanaConsultas";
            this.Text = "VentanaConsultas";
            this.GrupoResultado.ResumeLayout(false);
            this.GrupoResultado.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelTituloConsultar;
        private System.Windows.Forms.OpenFileDialog DialogoAbrirArchivo;
        private System.Windows.Forms.Button BtnAbrirArchivo;
        private System.Windows.Forms.GroupBox GrupoResultado;
        private System.Windows.Forms.Label LabelIdentificador;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
    }
}