﻿using Gestion_Alumnos_Profesorado_Asignaturas.Controladores;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestion_Alumnos_Profesorado_Asignaturas.PanelesCategorias
{
    class PanelAlumno: TableLayoutPanel
    {
        //
        // Atributos
        //
        private Color BG_COLOR = ColorUtils.WHITE;
        private string DEFAULT_FILENAME = "Alumnos";

        // Controles
        private TextBox InputDni;
        private TextBox InputNombre;
        private DateTimePicker InputFecha;
        private TextBox InputDireccion;
        private TextBox InputTelefono;
        private Panel PanelInfoPersonal;
        private Panel PanelInfoContacto;

        //
        // Constructor
        //
        public PanelAlumno()
        {
            StylePanel();
            InitComponents();
        }

        //
        // Getters
        //
        public Color getBgColor() { return BG_COLOR; }

        //
        // Setters
        //
        public void setBgColor(Color color) { this.BG_COLOR = color; }

        //
        // Otros métodos
        //
        private void StylePanel()
        {
            this.Dock = DockStyle.Fill;
            this.ColumnCount = 2;
            this.BackColor = BG_COLOR;
            this.AutoScroll = true;

        }
        private void InitComponents()
        {

            Label tituloCategoria = new Label()
            {
                Text = "Alumnos",
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 20),
                AutoSize = true,
                TextAlign = ContentAlignment.TopCenter,
                Dock = DockStyle.Fill,
                Padding = new Padding(0, 15, 0, 15)
            };

            // Info group
            GroupBox grupoInfoPersonal = new GroupBox()
            {
                Text = "Información personal",
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 8),
                Dock = DockStyle.Fill,
                AutoSize = true
            };
            PanelInfoPersonal = new Panel()
            {
                Dock = DockStyle.Fill,
                Padding = new Padding(0, 10, 0, 250),
                AutoScroll = true
            };

                // Labels
                Label dni = new Label()
                {
                    Text = "DNI:",
                    ForeColor = ColorUtils.DARK_BLUE,
                    Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                    AutoSize = true,
                    Dock = DockStyle.Left,
                    Padding = new Padding(4),
                    Anchor = AnchorStyles.None,
                    Location = new Point(base.Width / 2 - Width, Location.Y)
                };
                Label nombre = new Label()
                {
                    Text = "Nombre:",
                    ForeColor = ColorUtils.DARK_BLUE,
                    Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                    AutoSize = true,
                    Padding = new Padding(4),
                    Anchor = AnchorStyles.None,
                    Location = new Point(dni.Location.X, dni.Location.Y + dni.Height + 10)
                };
                Label fechaNacimiento = new Label()
                {
                    Text = "Fecha\nnacimiento:",
                    ForeColor = ColorUtils.DARK_BLUE,
                    Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                    AutoSize = true,
                    Padding = new Padding(4),
                    Anchor = AnchorStyles.None,
                    Location = new Point(nombre.Location.X, nombre.Location.Y + nombre.Height)
                };

                // Inputs
                InputDni = new TextBox()
                {
                    ForeColor = ColorUtils.DARK_BLUE,
                    Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                    Width = 200,
                    Padding = new Padding(4),
                    Anchor = AnchorStyles.None,
                    Location = new Point(dni.Location.X + dni.Width, dni.Location.Y)
                };
                InputNombre = new TextBox()
                {
                    ForeColor = ColorUtils.DARK_BLUE,
                    Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                    Width = 200,
                    Padding = new Padding(4),
                    Anchor = AnchorStyles.None,
                    Location = new Point(nombre.Location.X + nombre.Width, nombre.Location.Y)
                };
                InputFecha = new DateTimePicker()
                {
                    ForeColor = ColorUtils.DARK_BLUE,
                    Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                    Format = DateTimePickerFormat.Short,
                    Padding = new Padding(4),
                    Anchor = AnchorStyles.None,
                    Location = new Point(fechaNacimiento.Location.X + fechaNacimiento.Width, fechaNacimiento.Location.Y + fechaNacimiento.Height/2 - 1)
                };

            // Contact group
            GroupBox grupoInfoContacto = new GroupBox()
            {
                Text = "Información de contacto",
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 8),
                Dock = DockStyle.Fill,
                AutoSize = true
            };
            PanelInfoContacto = new Panel()
            {
                Dock = DockStyle.Fill,
                Padding = new Padding(0, 10, 0, 250),
                AutoScroll = true
            };
                // Labels
                Label direccion = new Label()
                {
                    Text = "Dirección:",
                    ForeColor = ColorUtils.DARK_BLUE,
                    Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                    AutoSize = true,
                    Dock = DockStyle.Left,
                    Padding = new Padding(4),
                    Anchor = AnchorStyles.None,
                    Location = new Point(base.Width / 2 - Width, Location.Y)
                };
                Label telefono = new Label()
                {
                    Text = "Teléfono:",
                    ForeColor = ColorUtils.DARK_BLUE,
                    Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                    AutoSize = true,
                    Padding = new Padding(4),
                    Anchor = AnchorStyles.None,
                    Location = new Point(dni.Location.X, dni.Location.Y + dni.Height + 10)
                };

                // Inputs
                InputDireccion = new TextBox()
                {
                    ForeColor = ColorUtils.DARK_BLUE,
                    Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                    Width = 200,
                    Padding = new Padding(4),
                    Anchor = AnchorStyles.None,
                    Location = new Point(direccion.Location.X + direccion.Width, direccion.Location.Y)
                };
                InputTelefono = new TextBox()
                {
                    ForeColor = ColorUtils.DARK_BLUE,
                    Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                    Width = 200,
                    Padding = new Padding(4),
                    Anchor = AnchorStyles.None,
                    Location = new Point(telefono.Location.X + telefono.Width, telefono.Location.Y)
                };

            // Save & Clear
            Panel PanelBotones = new Panel()
            {
                Dock = DockStyle.Fill,
                AutoScroll = true
            };
                // Buttons
                Button BtnGuardar = new Button()
                {
                    Text = "Guardar",
                    ForeColor = ColorUtils.DARK_BLUE,
                    Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                    AutoSize = true,
                    Anchor = AnchorStyles.None,
                    TextAlign = ContentAlignment.MiddleRight,
                    Location = new Point(Location.X - 15, Location.Y)
                };
            BtnGuardar.Click += BtnGuardar_Click;

                Button BtnLimpiar = new Button()
                {
                    Text = "Limpiar",
                    ForeColor = ColorUtils.DARK_BLUE,
                    Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                    AutoSize = true,
                    Anchor = AnchorStyles.None,
                    TextAlign = ContentAlignment.MiddleLeft,
                    Location = new Point(BtnGuardar.Location.X + BtnGuardar.Width + 25, Location.Y)
                };
            BtnLimpiar.Click += BtnLimpiar_Click;


            // Add title
            this.RowStyles.Add(new RowStyle(SizeType.Percent, 15));
            this.Controls.Add(tituloCategoria);
            this.SetColumnSpan(tituloCategoria, 2);

            // Add Info Group & items
            grupoInfoPersonal.Controls.Add(PanelInfoPersonal);
            PanelInfoPersonal.Controls.Add(dni);
            PanelInfoPersonal.Controls.Add(InputDni);
            PanelInfoPersonal.Controls.Add(nombre);
            PanelInfoPersonal.Controls.Add(InputNombre);
            PanelInfoPersonal.Controls.Add(fechaNacimiento);
            PanelInfoPersonal.Controls.Add(InputFecha);
            this.RowStyles.Add(new RowStyle(SizeType.Percent, 40));
            this.Controls.Add(grupoInfoPersonal);
            this.SetColumnSpan(grupoInfoPersonal, 2);

            // Add Contact Group & items
            grupoInfoContacto.Controls.Add(PanelInfoContacto);
            PanelInfoContacto.Controls.Add(direccion);
            PanelInfoContacto.Controls.Add(InputDireccion);
            PanelInfoContacto.Controls.Add(telefono);
            PanelInfoContacto.Controls.Add(InputTelefono);
            this.RowStyles.Add(new RowStyle(SizeType.Percent, 30));
            this.Controls.Add(grupoInfoContacto);
            this.SetColumnSpan(grupoInfoContacto, 2);

            // Add Save & Clear
            this.RowStyles.Add(new RowStyle(SizeType.Percent, 15));
            this.Controls.Add(PanelBotones);
            PanelBotones.Controls.Add(BtnGuardar);
            PanelBotones.Controls.Add(BtnLimpiar);
            this.SetColumnSpan(PanelBotones, 2);


        }

        private void BtnLimpiar_Click(object sender, EventArgs e)
        {
            PanelInfoPersonal.LimpiarCampos();
            PanelInfoContacto.LimpiarCampos();
        }

        public bool IsInfoOK()
        {
            // Else if en vez de ifs para evitar mostrar demasiados errores al usuario de una sola vez.

            // Dni
            if (!InputDni.Text.Trim().Coincide(ComparadorRegex.DNI)) {
                ComparadorRegex.MostrarErrorCoincidencia("El DNI introducido no posee el formato válido.\n" +
                                                         "\nFormato:\n" +
                                                         " - 00000000A\n" +
                                                         " - 00000000-A\n" +
                                                         " - 1111111A\n" +
                                                         " - 1111111-A", ComparadorRegex.ERROR_PATTERN);
                return false;
            }

            // Nombre
            if (!InputNombre.Text.Trim().Coincide(ComparadorRegex.ALPHABETIC_WORDS))
            {
                ComparadorRegex.MostrarErrorCoincidencia("El nombre sólo puede poseer letras y \"_\".\n" +
                                                        "No puede estar vacío ni se permiten números o caracteres especiales como (@, /, $).",
                                                        ComparadorRegex.ERROR_PATTERN);
                return false;
            }

            // Dirección
            if (String.IsNullOrEmpty(InputDireccion.Text.Trim()))
            {
                ComparadorRegex.MostrarErrorCoincidencia("El campo DIRECCIÓN no puede ser vacío.",
                                                        ComparadorRegex.ERROR_OTHER);
                return false;
            }

            // Teléfono
            if (!InputTelefono.Text.Trim().Coincide(ComparadorRegex.PHONE))
            {
                ComparadorRegex.MostrarErrorCoincidencia("El teléfono indicado no tiene un formato correcto.",
                                                        ComparadorRegex.ERROR_PATTERN);
                return false;
            }
            
            return true;


        }

        public string ObjectFileFormat() {
            string result = null;

            if (IsInfoOK()) {
                result = "\r\n-----\r\n" +
                         "DNI: " + InputDni.Text.Trim() + ",\r\n" +
                         "Name: " + InputNombre.Text.Trim() + ",\r\n" +
                         "BirthDate: " + InputFecha.Text.Trim() + ",\r\n" +
                         "Address: " + InputDireccion.Text.Trim() + ",\r\n" +
                         "Telf: " + InputTelefono.Text.Trim() + "\r\n";
            }

            return result;
        }

        //
        // Listeners
        //
        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            if (IsInfoOK()) {
                SaveFileDialog sfd = new SaveFileDialog() {
                    FileName = DEFAULT_FILENAME,
                    OverwritePrompt = false,
                    Filter = "Archivo de texto plano (*.txt) | *.txt"
                };

                if (sfd.ShowDialog() == DialogResult.OK)
                    GestorArchivos.EscribirArchivo(sfd.FileName, ObjectFileFormat());

            }
        }
    }
}
