﻿using Gestion_Alumnos_Profesorado_Asignaturas.Controladores;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Gestion_Alumnos_Profesorado_Asignaturas.PanelesCategorias
{
    class PanelAsignatura: TableLayoutPanel
    {
        //
        // Atributos
        //
        private Color BG_COLOR = ColorUtils.WHITE;
        private string DEFAULT_FILENAME = "Asignaturas";

        // Controles
        private TextBox InputID;
        private TextBox InputNombre;
        private ComboBox InputDepartamento;
        private NumericUpDown SelectorHoras;
        private Panel PanelInfoPersonal;
        private Panel PanelInfoGeneral;


        //
        // Constructor
        //
        public PanelAsignatura()
        {
            StylePanel();
            InitComponents();
        }

        //
        // Getters
        //
        public Color getBgColor() { return BG_COLOR; }

        //
        // Setters
        //
        public void setBgColor(Color color) { this.BG_COLOR = color; }

        //
        // Otros métodos
        //
        private void StylePanel()
        {
            this.Dock = DockStyle.Fill;
            this.ColumnCount = 2;
            this.BackColor = BG_COLOR;
            this.AutoScroll = true;

        }
        private void InitComponents()
        {

            Label tituloCategoria = new Label()
            {
                Text = "Asignaturas",
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 20),
                AutoSize = true,
                TextAlign = ContentAlignment.TopCenter,
                Dock = DockStyle.Fill,
                Padding = new Padding(0, 15, 0, 15)
            };

            // Info group
            GroupBox grupoInfoPersonal = new GroupBox()
            {
                Text = "Información Identificativa",
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 8),
                Dock = DockStyle.Fill,
                AutoSize = true
            };
            PanelInfoPersonal = new Panel()
            {
                Dock = DockStyle.Fill,
                Padding = new Padding(0, 10, 0, 250),
                AutoScroll = true
            };

            // Labels
            Label ID = new Label()
            {
                Text = "ID:",
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                AutoSize = true,
                Dock = DockStyle.Left,
                Padding = new Padding(4),
                Anchor = AnchorStyles.None,
                Location = new Point(base.Width / 2 - Width, Location.Y)
            };
            Label nombre = new Label()
            {
                Text = "Nombre:",
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                AutoSize = true,
                Padding = new Padding(4),
                Anchor = AnchorStyles.None,
                Location = new Point(ID.Location.X, ID.Location.Y + ID.Height + 10)
            };

            // Inputs
            InputID = new TextBox()
            {
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                Width = 200,
                Padding = new Padding(4),
                Anchor = AnchorStyles.None,
                Location = new Point(ID.Location.X + ID.Width, ID.Location.Y)
            };
            InputNombre = new TextBox()
            {
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                Width = 200,
                Padding = new Padding(4),
                Anchor = AnchorStyles.None,
                Location = new Point(nombre.Location.X + nombre.Width, nombre.Location.Y)
            };

            // General Info group
            GroupBox GrupoInfoGeneral = new GroupBox()
            {
                Text = "Información general",
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 8),
                Dock = DockStyle.Fill,
                AutoSize = true
            };
            PanelInfoGeneral = new Panel()
            {
                Dock = DockStyle.Fill,
                AutoScroll = true
            };
            // Labels
            Label departamento = new Label()
            {
                Text = "Departamento:",
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                AutoSize = true,
                Padding = new Padding(4),
                Anchor = AnchorStyles.None,
                Location = new Point(base.Width / 2 - Width, Location.Y + 20)
            };
            Label horas = new Label()
            {
                Text = "Horas:",
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                AutoSize = true,
                Padding = new Padding(4),
                Anchor = AnchorStyles.None,
                Location = new Point(departamento.Location.X, departamento.Location.Y + departamento.Height)
            };
            // Inputs
            InputDepartamento = new ComboBox()
            {
                Text = "Selecciona o introduce un nuevo departamento.",
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                Width = 200,
                Padding = new Padding(4),
                Anchor = AnchorStyles.None,
                Sorted = true,
                Location = new Point(departamento.Location.X + departamento.Width + 15, departamento.Location.Y)
            };
            InputDepartamento.Items.AddRange(ComboBoxManager.ListadoDepartamentos.OrderBy(c => c).ToArray());
            InputDepartamento.DropDown += AdjustWidth_OnDropDown;
            InputDepartamento.KeyPress += InputEstudios_KeyPressed;

            SelectorHoras = new NumericUpDown()
            {
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                Width = 200,
                Maximum = 1000,
                Padding = new Padding(4),
                Anchor = AnchorStyles.None,
                Location = new Point(horas.Location.X + horas.Width, horas.Location.Y)
            };


            // Save & Clear
            Panel PanelBotones = new Panel()
            {
                Dock = DockStyle.Fill,
                AutoScroll = true
            };
            // Buttons
            Button BtnGuardar = new Button()
            {
                Text = "Guardar",
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                AutoSize = true,
                Anchor = AnchorStyles.None,
                TextAlign = ContentAlignment.MiddleRight,
                Location = new Point(Location.X - 15, Location.Y)
            };
            BtnGuardar.Click += BtnGuardar_Click;
            Button BtnLimpiar = new Button()
            {
                Text = "Limpiar",
                ForeColor = ColorUtils.DARK_BLUE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                AutoSize = true,
                Anchor = AnchorStyles.None,
                TextAlign = ContentAlignment.MiddleLeft,
                Location = new Point(BtnGuardar.Location.X + BtnGuardar.Width + 25, Location.Y)
            };
            BtnLimpiar.Click += BtnLimpiar_Click;


            // Add title
            this.RowStyles.Add(new RowStyle(SizeType.Percent, 15));
            this.Controls.Add(tituloCategoria);
            this.SetColumnSpan(tituloCategoria, 2);

            // Add Info Group & items
            grupoInfoPersonal.Controls.Add(PanelInfoPersonal);
            PanelInfoPersonal.Controls.Add(ID);
            PanelInfoPersonal.Controls.Add(InputID);
            PanelInfoPersonal.Controls.Add(nombre);
            PanelInfoPersonal.Controls.Add(InputNombre);
            PanelInfoPersonal.Controls.Add(departamento);
            PanelInfoPersonal.Controls.Add(InputDepartamento);
            this.RowStyles.Add(new RowStyle(SizeType.Percent, 40));
            this.Controls.Add(grupoInfoPersonal);
            this.SetColumnSpan(grupoInfoPersonal, 2);

            // Add Contact Group & items
            GrupoInfoGeneral.Controls.Add(PanelInfoGeneral);
            PanelInfoGeneral.Controls.Add(departamento);
            PanelInfoGeneral.Controls.Add(InputDepartamento);
            PanelInfoGeneral.Controls.Add(horas);
            PanelInfoGeneral.Controls.Add(SelectorHoras);
            this.RowStyles.Add(new RowStyle(SizeType.Percent, 30));
            this.Controls.Add(GrupoInfoGeneral);
            this.SetColumnSpan(GrupoInfoGeneral, 2);

            // Add Save & Clear
            this.RowStyles.Add(new RowStyle(SizeType.Percent, 15));
            this.Controls.Add(PanelBotones);
            PanelBotones.Controls.Add(BtnGuardar);
            PanelBotones.Controls.Add(BtnLimpiar);
            this.SetColumnSpan(PanelBotones, 2);


        }

        private void BtnLimpiar_Click(object sender, EventArgs e)
        {
            PanelInfoPersonal.LimpiarCampos();
            PanelInfoGeneral.LimpiarCampos();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            if (IsInfoOK())
            {
                SaveFileDialog sfd = new SaveFileDialog()
                {
                    FileName = DEFAULT_FILENAME,
                    OverwritePrompt = false,
                    Filter = "Archivo de texto plano (*.txt) | *.txt"
                };

                if (sfd.ShowDialog() == DialogResult.OK)
                    GestorArchivos.EscribirArchivo(sfd.FileName, ObjectFileFormat());

            }
        }

        public bool IsInfoOK()
        {
            // Else if en vez de ifs para evitar mostrar demasiados errores al usuario de una sola vez.

            // Dni
            if (!InputID.Text.Trim().Coincide(ComparadorRegex.NUMBER))
            {
                ComparadorRegex.MostrarErrorCoincidencia("El identificador introducido debe poseer sólo números del 0 al 9.", ComparadorRegex.ERROR_PATTERN);
                return false;
            }

            // Nombre
            if (!InputNombre.Text.Trim().Coincide(ComparadorRegex.ALPHABETIC_WORDS))
            {
                ComparadorRegex.MostrarErrorCoincidencia("El nombre sólo puede poseer letras y \"_\".\n" +
                                                        "No puede estar vacío ni se permiten números o caracteres especiales como (@, /, $).",
                                                        ComparadorRegex.ERROR_PATTERN);
                return false;
            }

            // Departamento
            if (String.IsNullOrEmpty(InputDepartamento.Text.Trim()))
            {
                ComparadorRegex.MostrarErrorCoincidencia("El campo DEPARTAMENTO no puede ser vacío.",
                                                        ComparadorRegex.ERROR_OTHER);
                return false;
            }

            // Teléfono
            if (!SelectorHoras.Text.Trim().Coincide(ComparadorRegex.NUMBER))
            {
                ComparadorRegex.MostrarErrorCoincidencia("El valor indicado debe ser un número.",
                                                        ComparadorRegex.ERROR_PATTERN);
                return false;
            }

            return true;


        }

        public string ObjectFileFormat()
        {
            string result = null;

            if (IsInfoOK())
            {
                result = "\r\n-----\r\n" +
                         "ID: " + InputID.Text.Trim() + ",\r\n" +
                         "Name: " + InputNombre.Text.Trim() + ",\r\n" +
                         "Department: " + InputDepartamento.Text.Trim() + ",\r\n" +
                         "NumberHours: " + SelectorHoras.Text.Trim() + "\r\n";
            }

            return result;
        }

        private void InputEstudios_KeyPressed(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                ComboBox comboBox = (ComboBox)sender;
                comboBox.AddItem(comboBox.Text);
            }
        }

        private void AdjustWidth_OnDropDown(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            comboBox.UpdateWidthToMaxItem();
        }
    }
}
