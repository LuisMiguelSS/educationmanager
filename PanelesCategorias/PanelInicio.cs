﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Gestion_Alumnos_Profesorado_Asignaturas.PanelesCategorias
{
    class PanelInicio : TableLayoutPanel
    {
        //
        // Atributos
        //
        private Color BG_COLOR = ColorUtils.DARK_BLUE;


        //
        //
        //
        public PanelInicio() {
            StylePanel();
            InitComponents();
        }

        //
        // Getters
        //
        public Color GetBGColor() { return BG_COLOR; }

        //
        // Setters
        //
        public void SetBGColor(Color color) { this.BG_COLOR = color; }

        //
        // Otros métodos
        //
        private void StylePanel() {
            this.Dock = DockStyle.Fill;
            this.BackColor = BG_COLOR;
        }
        private void InitComponents() {

            Label tituloCategoria = new Label() {
                Text = "Inicio",
                ForeColor = ColorUtils.WHITE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily,20),
                AutoSize = true,
                TextAlign = ContentAlignment.BottomCenter,
                Dock = DockStyle.Fill
            };
            Label hintUso = new Label() {
                Text = "Para empezar, selecciona una categoría del menú lateral o...",
                ForeColor = ColorUtils.WHITE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                AutoSize = true,
                TextAlign = ContentAlignment.MiddleCenter,
                Dock = DockStyle.Fill,
                Padding = new Padding(0,15,0,0)
            };
            Button BtnConsultarDatos = new Button() {
                Text = "Consulta información",
                ForeColor = ColorUtils.WHITE,
                BackColor = ColorUtils.ORANGE,
                Font = new Font(VentanaPrincipal.DefaultFont.FontFamily, 10),
                AutoSize = true,
                TextAlign = ContentAlignment.TopCenter,
                Anchor = AnchorStyles.None,
                Padding = new Padding(15)
            };
            BtnConsultarDatos.Click += BtnConsultarDatos_Click;

            PictureBox arrowPicture = new PictureBox() {
                Name = "Arrow",
                Image = Properties.Resources.arrow,
                Width = 100,
                BackgroundImageLayout = ImageLayout.Zoom,
                Dock = DockStyle.Fill,
                Anchor = AnchorStyles.None
            };

            // Add title
            this.RowStyles.Add(new RowStyle(SizeType.Percent, 40));
            this.Controls.Add(tituloCategoria);
            this.RowStyles.Add(new RowStyle(SizeType.Percent, 15));
            this.Controls.Add(hintUso);
            this.RowStyles.Add(new RowStyle(SizeType.Percent, 15));
            this.Controls.Add(BtnConsultarDatos);
            this.RowStyles.Add(new RowStyle(SizeType.Percent, 30));
            this.Controls.Add(arrowPicture);
        }

        private void BtnConsultarDatos_Click(object sender, EventArgs e)
        {
            new VentanaConsultas().Show();
        }
    }
}
