﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestion_Alumnos_Profesorado_Asignaturas.Controladores
{
    static class ComboBoxManager
    {

        //
        // Atributos
        //
        public static List<string> ListadoEstudios = new List<string>() {
                    "Filología Inglesa",
                    "Matemáticas",
                    "Ingeniería Informática",
                    "Estudios Religiosos",
                    "Biología",
                    "Química",
                    "Física",
                    "Ingeniería de Diseño"
        };

        public static List<string> ListadoDepartamentos = new List<string>() {
                    "Música",
                    "Matemáticas",
                    "TIC",
                    "Tecnología",
                    "Biología y Geología",
                    "Idiomas",
                    "Física y Química",
                    "Arte"
        };


        //
        // Otros métodos
        //
        public static void UpdateWidthToMaxItem(this ComboBox combo) {
            int vertScrollBarWidth = combo.Items.Count > combo.MaxDropDownItems ?
                                            SystemInformation.VerticalScrollBarWidth
                                            : 0;

            int newWidth;
            foreach (string s in combo.Items)
            {
                newWidth = (int)combo.CreateGraphics()
                                .MeasureString(s, combo.Font).Width + vertScrollBarWidth;

                if (combo.DropDownWidth < newWidth)
                    combo.DropDownWidth = newWidth;
            }
        }

        public static void AddItem(this ComboBox combo, string text) {
            bool itemExists = false;
            foreach (string item in combo.Items)
            {
                if (item.ToLower().Equals(combo.Text.ToLower()))
                    itemExists = true;
            }

            // Añadir a lista
            if (!itemExists && !String.IsNullOrEmpty(combo.Text))
                combo.Items.Add(text);
        }
    }
}
