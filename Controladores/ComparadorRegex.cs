﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestion_Alumnos_Profesorado_Asignaturas.Controladores
{
    static class ComparadorRegex
    {
        //
        // Atributos
        //
        public const string ALPHABETIC = @"^[A-z]+$";
        public const string ALPHABETIC_WORDS = @"[\w ]+";
        public const string NUMBER = @"^\d+$";
        public const string DNI = @"\d{7,8}-?[A-z]$";
        public const string PHONE = @"([+(\d]{1})(([\d+() -.]){5,16})([+(\d]{1})";

        // Tipos de datos (Alumno, Profesor y Asignatura)
        public const string FORMATO_ALUMNO = @"DNI:(?<dni>[^\r\n]+)\s+Name:(?<name>[^\r\n]+)\s+BirthDate:(?<birthdate>[^\r\n]+)\s+Address:(?<address>[^\r\n]+)\s+Telf:(?<telf>[^\r\n]+)";
        public const string FORMATO_PROFESOR= @"DNI:(?<dni>[^\r\n]+)\s+Name:(?<name>[^\r\n]+)\s+Specialty:(?<specialty>[^\r\n]+)\s+Address:(?<address>[^\r\n]+)\s+Telf:(?<telf>[^\r\n]+)";
        public const string FORMATO_ASIGNATURA= @"ID:(?<id>[^\r\n]+)\s+Name:(?<name>[^\r\n]+)\s+Department:(?<department>[^\r\n]+)\s+NumberHours:(?<numberhours>[^\r\n]+)";


        // Errores
        public const int ERROR_OTHER = -1;
        public const int ERROR_AZ = 0;
        public const int ERROR_09 = 1;
        public const int ERROR_PATTERN = 2;

        //
        // Otros métodos
        //
        public static bool Coincide(this string text, string format) {
            Match match = Regex.Match(text, format);

            return match.Success ? true : false;
        }

        //
        // Diálogos
        //
        public static void MostrarErrorCoincidencia(string message, int ErrorType = ERROR_OTHER, bool severe = false)
        {
            string TituloError = "";
            MessageBoxIcon icon = severe ? MessageBoxIcon.Error : MessageBoxIcon.Warning;

            // Título
            switch (ErrorType) {
                case ERROR_OTHER:
                    TituloError = "Error";
                    break;
                case ERROR_AZ:
                    TituloError = "Error de formato alfabético";
                    break;
                case ERROR_09:
                    TituloError = "Error de formato numérico";
                    break;
                case ERROR_PATTERN:
                    TituloError = "Error de formato";
                    break;
                default:
                    TituloError = "Advertencia";
                    break;
            }

            // Mostrar
            MessageBox.Show(message, TituloError,MessageBoxButtons.OK, icon);
        }

    }
}
