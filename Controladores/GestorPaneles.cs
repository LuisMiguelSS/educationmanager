﻿using System;
using System.Windows.Forms;

namespace Gestion_Alumnos_Profesorado_Asignaturas.Controladores
{
    static class GestorPaneles
    {
        public static void LimpiarCampos(this Panel panel)
        {
            foreach (Control control in panel.Controls)
            {
                if (control.GetType() == typeof(TextBox) ||
                    control.GetType() == typeof(ComboBox))
                    control.Text = string.Empty;
                else if (control.GetType() == typeof(DateTimePicker))
                    control.Text = DateTime.Now.ToShortDateString();
                else if (control.GetType() == typeof(NumericUpDown))
                    control.Text = "0";
            }
        } // end of method.


    }
}
