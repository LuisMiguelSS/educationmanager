﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Gestion_Alumnos_Profesorado_Asignaturas.Controladores
{
    class GestorArchivos
    {
        //
        // Otros métodos
        //
        public static void EscribirArchivo(string rutaArchivo, string text, bool append = true)
        {
            StreamWriter archivo = null;

            try
            {
                if (File.Exists(rutaArchivo))
                {

                    if (append)
                        File.AppendAllText(rutaArchivo, text);
                    else
                    {
                        archivo = File.CreateText(rutaArchivo);
                        archivo.WriteLine(text);
                    }

                    if (archivo != null)
                        archivo.Close();
                }
                else
                {
                    archivo = File.CreateText(rutaArchivo);
                    archivo.WriteLine(text);

                    if (archivo != null)
                        archivo.Close();
                }

            }
            catch (PathTooLongException)
            {
                MessageBox.Show("La ruta indicada hacia el archivo es demasiado larga.");
            }
            catch (DirectoryNotFoundException)
            {
                MessageBox.Show("La carpeta indicada no existe.");
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("No se puede acceder al archivo porque los permisos que se le han establecido lo prohíben.");
            }
            catch (IOException)
            {
                MessageBox.Show("Ha ocurrido un error intentando leer/escribir al archivo.\n" +
                                " ¿Quizás hay algún otro programa utilizándolo?");
            }

        }// end of method.


    }
}
