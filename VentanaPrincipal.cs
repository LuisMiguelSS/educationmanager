﻿using Gestion_Alumnos_Profesorado_Asignaturas.PanelesCategorias;
using System;
using System.Drawing;
using System.Windows.Forms;


namespace Gestion_Alumnos_Profesorado_Asignaturas
{
    public partial class VentanaPrincipal : Form
    {
        //
        // Atributos
        //
        private static Label categoriaSeleccionada = null;

        //
        // Constructor
        //
        public VentanaPrincipal()
        {
            InitializeComponent();
            SelectAndLoadCategory(labelCategoriaInicio);
        }

        //
        // Otros Métodos
        //
        private void SelectAndLoadCategory(Label label)
        {

            // Deseleccionar el resto si es que hay alguna seleccionada.
            foreach (Control item in MenuLateral.Controls)
            {
                if (item.GetType() == typeof(Label) && item.BackColor == ColorUtils.DARK_BLUE)
                    ReestablecerColorCategoria((Label)item);
            }


            // Establecer como "seleccionada".
            OscurecerCategoria(label);
            categoriaSeleccionada = label;

            // Limpiar panel.
            PanelPrincipal.Controls.Clear();

            // Cargar contenido.
            switch (label.Text)
            {
                case "Inicio":
                    PanelPrincipal.Controls.Add(new PanelInicio());
                    break;
                case "Alumnos":
                    PanelPrincipal.Controls.Add(new PanelAlumno());
                    break;
                case "Asignaturas":
                    PanelPrincipal.Controls.Add(new PanelAsignatura());
                    break;
                case "Profesores":
                    PanelPrincipal.Controls.Add(new PanelProfesor());
                    break;

                default: break;
            } 
        }

        private void OscurecerCategoria(Label categoria) {
            categoria.BackColor = ColorUtils.DARK_BLUE;
            categoria.ForeColor = ColorUtils.WHITE;
            categoria.Image = ((Bitmap)categoria.Image).Recolor(ColorUtils.WHITE);
        }
        private void ReestablecerColorCategoria(Label categoria) {
            categoria.BackColor = ColorUtils.WHITE;
            categoria.ForeColor = ColorUtils.DARK_BLUE;
            categoria.Image = ((Bitmap)categoria.Image).Recolor(ColorUtils.DARK_BLUE);
        }

        //
        // Listeners
        //
        private void ItemCategoria_Hover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
            OscurecerCategoria((Label)sender);

        }
        private void ItemCategoria_HoverOut(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            Label label = (Label)sender;

            if (categoriaSeleccionada != label)
                ReestablecerColorCategoria(label);
        }

        private void ItemCategoria_Click(object sender, EventArgs e)
        {

            switch (((Label)sender).Text)
            {
                case "Inicio": SelectAndLoadCategory(labelCategoriaInicio);
                    break;
                case "Alumnos": SelectAndLoadCategory(labelCategoriaAlumno);
                    break;
                case "Asignaturas": SelectAndLoadCategory(labelCategoriaAsignatura);
                    break;
                case "Profesores": SelectAndLoadCategory(labelCategoriaProfesor);
                    break;

                default: break;
            }
        }

        private void labelSalir_Click(object sender, EventArgs e) {
            Close();
        }

        private void ventanaPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro de que deseas salir?", "Confirmación de salida", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.No)
                e.Cancel = true;
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
