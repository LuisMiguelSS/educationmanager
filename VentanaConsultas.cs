﻿using Gestion_Alumnos_Profesorado_Asignaturas.Categorias;
using Gestion_Alumnos_Profesorado_Asignaturas.Controladores;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Gestion_Alumnos_Profesorado_Asignaturas
{
    public partial class VentanaConsultas : Form
    {
        //
        // Atributo
        //
        public string textoArchivo;
        public string tipoObjetos;
        private List<string> datosObjeto;
        public VentanaConsultas()
        {
            textoArchivo = @"";
            tipoObjetos = "";
            datosObjeto = new List<string>();
            InitializeComponent();
        }

        private void BtnAbrirArchivo_Click(object sender, EventArgs e)
        {
            // Abrir archivo
            OpenFileDialog ofd = new OpenFileDialog() {
                Filter = "Archivos de texto plano (*.txt) | *.txt"
            };

            if (ofd.ShowDialog() == DialogResult.OK) {

                try
                {
                    string[] LinesText = File.ReadAllLines(ofd.FileName);

                    textoArchivo = "";
                    // Cargar texto en variable
                    foreach (string line in LinesText)
                        textoArchivo += line;
                }
                catch (IOException) {
                    MessageBox.Show("Ha ocurrido un error de lectura al intentar arbrir el archivo.");
                }
            }

            // Habilitar controles
            GrupoResultado.Enabled = false; // Puesto en false ya que lo que hay abajo no funciona

            // Identificar tipos de objetos almacenados
            if (textoArchivo.Contains("BirthDate"))
                tipoObjetos = "Alumno";
            else if (textoArchivo.Contains("Specialty"))
                tipoObjetos = "Profesor";
            else if (textoArchivo.Contains("Department"))
                tipoObjetos = "Asignatura";

            // Buscar resultados
            if (GrupoResultado.Enabled) {

                //Obtener resultados usando regex
                /*var alumnos = Regex.Matches(textoArchivo, ComparadorRegex.FORMATO_ALUMNO)
                                        .OfType<Match>()
                                        .Select(item => new Alumno()
                                        {
                                            Dni = item.Groups["dni"].Value,
                                            Name = item.Groups["name"].Value,
                                            BirthDate = item.Groups["birthdate"].Value,
                                            Telf = item.Groups["telf"].Value,
                                            Address = item.Groups["address"].Value
                                        })
                                        .ToList();

                // Segunda alternativa ->
                //Regex r = new Regex(ComparadorRegex.FORMATO_ALUMNO, RegexOptions.IgnoreCase);
                //Match m = r.Match(textoArchivo);
                //
                //while (m.Success) {
                //    m.Value.OfType<Match>
                //}

                MessageBox.Show(alumnos[0].ToString());

                if (alumnos != null && alumnos.Count > 0)
                {
                    MessageBox.Show("Works");
                    label1.Text += alumnos[0].Dni;
                    label1.Text += alumnos[0].Name;
                    label1.Text += alumnos[0].BirthDate;
                    label1.Text += alumnos[0].Telf;
                    label1.Text += alumnos[0].Address;
                }*/
            }

        }


    }
}
