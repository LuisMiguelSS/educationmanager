﻿using System.Drawing;

namespace Gestion_Alumnos_Profesorado_Asignaturas
{
    static class ColorUtils
    {
        //
        // Atributos
        //
        public static Color WHITE = Color.FromArgb(250, 252, 254);
        public static Color DARK = Color.FromArgb(0, 3, 3);
        public static Color BLUE = Color.FromArgb(4, 139, 168);
        public static Color ORANGE = Color.FromArgb(255, 107, 53);
        public static Color DARK_BLUE = Color.FromArgb(23, 30, 36);
        public static Color HOME_BG = Color.FromArgb(80, 91, 106);

        /**
         * Llama a su método principal Recolor(img, Color, Color) pero en su caso
         * pasándole 'null' como 'oldColor'.
         */
        public static Bitmap Recolor(this Bitmap img, Color newColor) {
            return Recolor(img, null, newColor);
        }
        /**
         * Este método recibe un Bitmap, un color antiguo y el nuevo para reemplazar.
         * En el caso en el que reciba 'null' como 'oldColor', reemplazará cualquier pixel
         * que no sea transparente al nuevo color.
         */
        public static Bitmap Recolor(this Bitmap img, Color? oldColor, Color newColor) {
            for (int x = 0; x < img.Width; x++)
            {
                for (int y = 0; y < img.Height; y++)
                {
                    if (oldColor == null) {
                        if (img.GetPixel(x, y).A != 0)
                            img.SetPixel(x, y, newColor);

                    } else
                        img.SetPixel(x, y, newColor);
                }
            }
            return img;
        }


    }
}
