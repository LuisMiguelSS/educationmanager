﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestion_Alumnos_Profesorado_Asignaturas.Categorias
{
    public class Alumno
    {
        public string Dni { get; set; }
        public string Name { get; set; }
        public string BirthDate { get; set; }
        public string Telf { get; set; }
        public string Address { get; set; }
    }
}
